*** Settings ***
Library    SeleniumLibrary
Library    WebDriverManager.py
Library    OperatingSystem
Library    XvfbRobot

*** Variables ***
${BROWSER}    chrome

*** Test Cases ***
Test Web Page
  [Documentation]    script opens browser and launch www.ranorex.com and verifies the title
   #set web driver    ${BROWSER}
   Launch Application

*** Keywords ***
Launch Application
    ${chrome_options}=    Evaluate    sys.modules['selenium.webdriver'].ChromeOptions()    sys
    Call Method    ${chrome_options}    add_argument    test-type
    Call Method    ${chrome_options}    add_argument    --disable-extensions
    Call Method    ${chrome_options}    add_argument    --headless
    Call Method    ${chrome_options}    add_argument    --disable-gpu
    Call Method    ${chrome_options}    add_argument    --no-sandbox
    Create Webdriver    Chrome    chrome_options=${chrome_options}
    Set Window Size    1920    1080
    Go To    https://www.ranorex.com
    Wait Until Page Contains    Test Automation for All
    Close Browser