#!/usr/bin/env python
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from webdriver_manager.opera import OperaDriverManager
from webdriver_manager.microsoft import IEDriverManager
import os

def set_web_driver(browser):
    if browser == 'opera':
        s = OperaDriverManager().install()
        s = s.replace('operadriver.exe', '')
        os.environ['PATH'] = os.environ['PATH'] + ';' + s

    elif browser == 'ie':
        s = IEDriverManager().install()
        s = s.replace('IEDriverServer.exe', '')
        os.environ['PATH'] = os.environ['PATH'] + ';' + s

    elif browser == 'chrome':
        s = ChromeDriverManager().install()
        s = s.replace('chromedriver.exe', '')
        os.environ['PATH'] = os.environ['PATH'] + ';' + s
