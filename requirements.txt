robotframework==3.2.1
robotframework-seleniumlibrary==4.4.0
selenium==3.141.0
PyYAML==5.3.1
keyboard==0.13.5
webdriver-manager==3.2.1
robotframework-xvfb==1.2.2
xvfbwrapper==0.2.9